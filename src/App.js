// * ignored current file from prettier
// * look at the [ ${projectRoot}/.prettierignore ] file

import { BrowserRouter, Route, Routes } from 'react-router-dom'
import HomeAll from './Home'
import paths from './paths'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={paths.home.root} element={<HomeAll.Home />}>
          <Route index={true} element={<HomeAll.HomeIndex />}></Route>

          <Route path={paths.home.parent.root} element={<HomeAll.ParentAll.Parent />}>
            <Route index={true} element={<HomeAll.ParentAll.ParentIndex />}></Route>
            <Route path={paths.home.parent.grade1.root} element={<HomeAll.ParentAll.Grade1All.Grade1 />}>
              <Route index={true} element={<HomeAll.ParentAll.Grade1All.Grade1Index />}></Route>
            </Route>
            <Route path={paths.home.parent.grade2.root} element={<HomeAll.ParentAll.Grade2All.Grade2 />}>
              <Route index={true} element={<HomeAll.ParentAll.Grade2All.Grade2Index />}></Route>
            </Route>
            <Route path={paths.home.parent.grade3.root} element={<HomeAll.ParentAll.Grade3All.Grade3 />}>
              <Route index={true} element={<HomeAll.ParentAll.Grade3All.Grade3Index />}></Route>
            </Route>
            <Route path={paths.home.parent.grade4.root} element={<HomeAll.ParentAll.Grade4All.Grade4 />}>
              <Route index={true} element={<HomeAll.ParentAll.Grade4All.Grade4Index />}></Route>
            </Route>
          </Route>

          <Route path={paths.home.student.root} element={<HomeAll.StudentAll.Student />}>
            <Route index={true} element={<HomeAll.StudentAll.StudentIndex />}></Route>
            <Route path={paths.home.student.grade1.root} element={<HomeAll.StudentAll.Grade1All.Grade1 />}>
              <Route index={true} element={<HomeAll.StudentAll.Grade1All.Grade1Index />}></Route>
            </Route>
            <Route path={paths.home.student.grade2.root} element={<HomeAll.StudentAll.Grade2All.Grade2 />}>
              <Route index={true} element={<HomeAll.StudentAll.Grade2All.Grade2Index />}></Route>
            </Route>
            <Route path={paths.home.student.grade3.root} element={<HomeAll.StudentAll.Grade3All.Grade3 />}>
              <Route index={true} element={<HomeAll.StudentAll.Grade3All.Grade3Index />}></Route>
            </Route>
            <Route path={paths.home.student.grade4.root} element={<HomeAll.StudentAll.Grade4All.Grade4 />}>
              <Route index={true} element={<HomeAll.StudentAll.Grade4All.Grade4Index />}></Route>
            </Route>
          </Route>

          <Route path={paths.home.supporter.root} element={<HomeAll.SupporterAll.Supporter />}>
            <Route index={true} element={<HomeAll.SupporterAll.SupporterIndex />}></Route>
            <Route path={paths.home.supporter.teamA.root} element={<HomeAll.SupporterAll.TeamAAll.TeamA />}>
              <Route index={true} element={<HomeAll.SupporterAll.TeamAAll.TeamAIndex />}></Route>
            </Route>
            <Route path={paths.home.supporter.teamB.root} element={<HomeAll.SupporterAll.TeamBAll.TeamB />}>
              <Route index={true} element={<HomeAll.SupporterAll.TeamBAll.TeamBIndex />}></Route>
            </Route>
          </Route>

          <Route path={paths.home.teacher.root} element={<HomeAll.TeacherAll.Teacher />}>
            <Route index={true} element={<HomeAll.TeacherAll.TeacherIndex />}></Route>
            <Route path={paths.home.teacher.classA.root} element={<HomeAll.TeacherAll.ClassAAll.ClassA />}>
              <Route index={true} element={<HomeAll.TeacherAll.ClassAAll.ClassAIndex />}></Route>
            </Route>
            <Route path={paths.home.teacher.classB.root} element={<HomeAll.TeacherAll.ClassBAll.ClassB />}>
              <Route index={true} element={<HomeAll.TeacherAll.ClassBAll.ClassBIndex />}></Route>
            </Route>
            <Route path={paths.home.teacher.classC.root} element={<HomeAll.TeacherAll.ClassCAll.ClassC />}>
              <Route index={true} element={<HomeAll.TeacherAll.ClassCAll.ClassCIndex />}></Route>
            </Route>
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
