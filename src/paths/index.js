const paths = {
  home: {
    root: '/',
    parent: {
      root: 'parent',
      grade1: {
        root: 'grade-1',
      },
      grade2: {
        root: 'grade-2',
      },
      grade3: {
        root: 'grade-3',
      },
      grade4: {
        root: 'grade-4',
      },
    },
    student: {
      root: 'student',
      grade1: {
        root: 'grade-1',
      },
      grade2: {
        root: 'grade-2',
      },
      grade3: {
        root: 'grade-3',
      },
      grade4: {
        root: 'grade-4',
      },
    },
    supporter: {
      root: 'supporter',
      teamA: {
        root: 'team-a',
      },
      teamB: {
        root: 'team-b',
      },
    },
    teacher: {
      root: 'teacher',
      classA: {
        root: 'class-a',
      },
      classB: {
        root: 'class-b',
      },
      classC: {
        root: 'class-c',
      },
    },
  },
}

export default paths
