import PropTypes from 'prop-types'

function PaddedButton(props) {
  const { children, onClick } = props

  return (
    <button style={{ padding: '1rem 5ch 1rem 5ch' }} onClick={onClick}>
      {children}
    </button>
  )
}

PaddedButton.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func,
}

export default PaddedButton
