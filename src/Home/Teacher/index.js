import { Outlet, useNavigate } from 'react-router-dom'
import ClassAAll from './ClassA'
import ClassBAll from './ClassB'
import ClassCAll from './ClassC'
import PaddedButton from '../../components/PaddedButton'
import paths from '../../paths'

function Teacher() {
  return <Outlet />
}

function TeacherIndex() {
  const navigate = useNavigate()

  return (
    <div>
      <div>
        <div>선생님</div>
      </div>
      <div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.teacher.classA.root)}
          >
            클래스 A 담당
          </PaddedButton>
        </div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.teacher.classB.root)}
          >
            클래스 B 담당
          </PaddedButton>
        </div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.teacher.classC.root)}
          >
            클래스 C 담당
          </PaddedButton>
        </div>
      </div>
    </div>
  )
}

const TeacherAll = {
  Teacher,
  TeacherIndex,
  ClassAAll,
  ClassBAll,
  ClassCAll,
}

export default TeacherAll
