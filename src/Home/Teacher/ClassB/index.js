import { Outlet } from 'react-router-dom'

function ClassB() {
  return <Outlet />
}

function ClassBIndex() {
  return <div>클래스 B 담당</div>
}

const ClassBAll = {
  ClassB,
  ClassBIndex,
}

export default ClassBAll
