import { Outlet } from 'react-router-dom'

function ClassA() {
  return <Outlet />
}

function ClassAIndex() {
  return <div>클래스 A 담당</div>
}

const ClassAAll = {
  ClassA,
  ClassAIndex,
}

export default ClassAAll
