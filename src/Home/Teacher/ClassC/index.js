import { Outlet } from 'react-router-dom'

function ClassC() {
  return <Outlet />
}

function ClassCIndex() {
  return <div>클래스 C 담당</div>
}

const ClassCAll = {
  ClassC,
  ClassCIndex,
}

export default ClassCAll
