import { Outlet, useNavigate } from 'react-router-dom'
import ParentAll from './Parent'
import StudentAll from './Student'
import SupporterAll from './Supporter'
import TeacherAll from './Teacher'
import PaddedButton from '../components/PaddedButton'
import paths from '../paths'

function Home() {
  return <Outlet />
}

function HomeIndex() {
  const navigate = useNavigate()

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
      }}
    >
      <div style={{ flexShrink: 1 }}>
        <PaddedButton onClick={() => navigate(paths.home.student.root)}>
          학생
        </PaddedButton>
      </div>
      <div style={{ flexShrink: 1 }}>
        <PaddedButton onClick={() => navigate(paths.home.teacher.root)}>
          선생님
        </PaddedButton>
      </div>
      <div style={{ flexShrink: 1 }}>
        <PaddedButton onClick={() => navigate(paths.home.supporter.root)}>
          지원팀
        </PaddedButton>
      </div>
      <div style={{ flexShrink: 1 }}>
        <PaddedButton onClick={() => navigate(paths.home.parent.root)}>
          학부모
        </PaddedButton>
      </div>
    </div>
  )
}

const HomeAll = {
  Home,
  HomeIndex,
  ParentAll,
  StudentAll,
  SupporterAll,
  TeacherAll,
}

export default HomeAll
