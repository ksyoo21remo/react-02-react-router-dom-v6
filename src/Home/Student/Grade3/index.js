import { Outlet } from 'react-router-dom'

function Grade3() {
  return <Outlet />
}

function Grade3Index() {
  return <div>3학년</div>
}

const Grade3All = {
  Grade3,
  Grade3Index,
}

export default Grade3All
