import { Outlet } from 'react-router-dom'

function Grade1() {
  return <Outlet />
}

function Grade1Index() {
  return <div>1학년</div>
}

const Grade1All = {
  Grade1,
  Grade1Index,
}

export default Grade1All
