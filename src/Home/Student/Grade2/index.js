import { Outlet } from 'react-router-dom'

function Grade2() {
  return <Outlet />
}

function Grade2Index() {
  return <div>2학년</div>
}

const Grade2All = {
  Grade2,
  Grade2Index,
}

export default Grade2All
