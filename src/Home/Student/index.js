import { Outlet, useNavigate } from 'react-router-dom'
import Grade1All from './Grade1'
import Grade2All from './Grade2'
import Grade3All from './Grade3'
import Grade4All from './Grade4'
import PaddedButton from '../../components/PaddedButton'
import paths from '../../paths'

function Student() {
  return <Outlet />
}

function StudentIndex() {
  const navigate = useNavigate()

  return (
    <div>
      <div>
        <div>학생</div>
      </div>
      <div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.student.grade1.root)}
          >
            1학년
          </PaddedButton>
        </div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.student.grade2.root)}
          >
            2학년
          </PaddedButton>
        </div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.student.grade3.root)}
          >
            3학년
          </PaddedButton>
        </div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.student.grade4.root)}
          >
            4학년
          </PaddedButton>
        </div>
      </div>
    </div>
  )
}

const StudentAll = {
  Student,
  StudentIndex,
  Grade1All,
  Grade2All,
  Grade3All,
  Grade4All,
}

export default StudentAll
