import { Outlet } from 'react-router-dom'

function Grade4() {
  return <Outlet />
}

function Grade4Index() {
  return <div>4학년</div>
}

const Grade4All = {
  Grade4,
  Grade4Index,
}

export default Grade4All
