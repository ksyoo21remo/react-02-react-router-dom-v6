import { Outlet } from 'react-router-dom'

function TeamA() {
  return <Outlet />
}

function TeamAIndex() {
  return <div>A 팀</div>
}

const TeamAAll = {
  TeamA,
  TeamAIndex,
}

export default TeamAAll
