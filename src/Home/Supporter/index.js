import { Outlet, useNavigate } from 'react-router-dom'
import TeamAAll from './TeamA'
import TeamBAll from './TeamB'
import PaddedButton from '../../components/PaddedButton'
import paths from '../../paths'

function Supporter() {
  return <Outlet />
}

function SupporterIndex() {
  const navigate = useNavigate()

  return (
    <div>
      <div>
        <div>지원팀</div>
      </div>
      <div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.supporter.teamA.root)}
          >
            A 팀
          </PaddedButton>
        </div>
        <div>
          <PaddedButton
            onClick={() => navigate(paths.home.supporter.teamB.root)}
          >
            B 팀
          </PaddedButton>
        </div>
      </div>
    </div>
  )
}

const SupporterAll = {
  Supporter,
  SupporterIndex,
  TeamAAll,
  TeamBAll,
}

export default SupporterAll
