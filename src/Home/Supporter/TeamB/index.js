import { Outlet } from 'react-router-dom'

function TeamB() {
  return <Outlet />
}

function TeamBIndex() {
  return <div>B 팀</div>
}

const TeamBAll = {
  TeamB,
  TeamBIndex,
}

export default TeamBAll
